import Vuex from 'vuex'
import Vue from 'vue'
import men from './men.module'
import women from './women.module'
import auth from './auth.module'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    men,
    women,
    auth,
  }
})
export default store
