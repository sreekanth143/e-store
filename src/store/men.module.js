import axios from "axios"

const men = {
    namespaced: true,
    state: {
      productData: null,
      singleProductData: null,
      similarProductData: [],
    },
    getters: {
      products(state) {
         return state.productData
      },
      singleProduct(state) {
        return state.singleProductData
      },
      similarProducts(state) {
        return state.similarProductData
      },
    },
    mutations: {
      ADD_TO_CART(state) {
         state.inCart.push(); 
        },
      setMenProductData(state, data) {
       state.productData = data
      },
      setSingleProductData(state, data) {
        state.singleProductData = data
      },
      setSimilarProductData(state, data) {
        state.similarProductData = data
      },
    },
    actions: {
      addToCart(context) { 
        context.commit('ADD_TO_CART');
      },
        getMenProducts({ commit }) {
          axios.get('https://dummyjson.com/products')
          .then(response => {
            console.log('response', response.data);
            this.products = response.data.products;
            commit('setMenProductData', response.data.products)
            
          })
          .catch(err => {
            console.log(err)
        })
        },
        getSingleProduct({ commit }, productid) {
          axios.get(`https://dummyjson.com/products/${productid}`)
          .then(response => {
            commit('setSingleProductData', response.data)
            console.log('response', response.data)
          })
          .catch(err=> {
            console.log(err)
        })
        },
        getSimilarProduct({ commit }) {
          axios.get('https://dummyjson.com/products?select=title,thumbnail&skip=0&limit=4')
          .then(response => {
            commit('setSimilarProductData', response.data.products)
            console.log('response', response.data.products)
          })
          .catch(err=> {
            console.log(err)
        })
        },
      },
    }
    export default men