import axios from "axios";
import router from '../router/index';
const user = {
    state: {
        user: null,
    },
    mutations: {
            updateAccessToken: (state, token) => {
                state.user.token = token;
            },
            updateUser(state, data) {
                state.user = data
            },
            logout: (state) => {
                state.token = null;
            },
        },
    actions: {
        login({ commit }, userData) {
            axios
              .post("https://dummyjson.com/auth/login", userData)
              .then((response) => {
                console.log(response.data);
                localStorage.setItem("token", response.data.token);
                commit("updateUser", response.data);
                commit('updateAccessToken', response.data.token);
                router.push("/");
              })
              .catch((error) => {
                console.log(error);
                commit("updateAccessToken", null);
              });
          },
          register({ commit }, userData) {
            axios
              .post("https://dummyjson.com/users/add", userData)
              .then((response) => {
                console.log(response.data);
                localStorage.setItem("token", response.data.token);
                commit("updateUser", response.data);
                commit('updateAccessToken', response.data.token);
                router.push("/");
              })
              .catch((error) => {
                console.log(error);
                commit("updateAccessToken", null);
              });
          },
           
          logout() {
            localStorage.removeItem("token");
            this.$store.dispatch("updateUser", null);
            this.$router.replace({ name: "Error" });
          },  
            updateUser({ commit }, data) {
                commit('updateUser', data);
            }
        
        
    },
    getters: {
        user(state) {
            return state.user
        }
    }, 
    }

export default user