import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router'
import store from './store'

Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  store,
  // created() {
  //   const token = localStorage.getItem('token')
  //   if (token) {
  //     store.dispatch('updateAccessToken', token)
  //     // dispatch action to fetch user data using the token
  //     store.dispatch("fetchAccessToken");
  //     // store.dispatch('actName', { token})
  //     store.dispatch()
  //   }
  // },
  render: h => h(App)
}).$mount('#app')
