import Vue from 'vue'
import Menproductdetails from '../components/Menproductdetails.vue'
import Womenproductdetails from '../components/Womenproductdetails.vue'
import VueRouter from 'vue-router'
import Search from '../components/Search.vue'
import Signup from '../components/Signup.vue'
import Login from '../components/Login.vue'
import Favorites from '../components/Favorites.vue'
import Home from '@/components/Home'
import Shop from '@/components/Shop'
import Men from '@/components/Men'
import Women from '@/components/Women'
import Error from '../components/Error.vue'
import Blog from '@/components/Blog'
import Post from '@/components/Post'
import Cart from '@/components/Cart'
import Navbar from '../components/Navbar.vue'
import Wishlist from '../components/Wishlist.vue'
import Payment from '../components/Payment.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/',
  component: Navbar,
  redirect: '/',
  children:[
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/signup",
    name: "Signup",
    component: Signup,
    props: true,
  },
  {
    path: '/forgot-password',
    name: 'ForgotPassword',
    component: () => import('../components/ForgotPassword.vue'),
    props: true,
    },
  {
    path: "/login",
    name: "Login",
    component: Login,
    props: true,
    beforeEnter(to, from, next) {
      //checking before the user clicks the login button
    console.log('login beforeEnter');
    console.log(to, from);
    next();
    },
    beforeEach(to, from, next) {
      //checking whether user is logged in or not 
      if (to.name !== 'Login' && !Login) next({ name: 'Login' })
      else next()
    },      
  },
  
  {
    path: "/blog",
    name: "Blog",
    component: Blog,
  },
  {
    path:'/shop',
    component:Shop,
    name:'Shop'
  },
  {
    path: "/men",
    name: "Men",
    component: Men,
    props: true,
  },
  {
    path: "/women",
    name: "Women",
    component: Women,
    props: true,
  },
  {
    path: "/home/men/Menproductdetails/:id",
    name: "Menproductdetails",
    component: Menproductdetails,
    props: true,
  },
  {
    path: "/home/women/Womenproductdetails/:id",
    name: "Womenproductdetails",
    component: Womenproductdetails,
    props: true,
  },
  {
    path: "/wishlist",
    name: "Wishlist",
    component: Wishlist,
    props: true,
  },
  {
    path: "/paymentsection",
    name: "Payment",
    component: Payment,
    props: true,
  },
  {
    path: "/post",
    name: "Post",
    component: Post,
    props: true,
  },
  {
    path: "/search",
    name: "Search",
    component: Search,
    props: true,
  },
  {
    path: "/cart",
    name: "Cart",
    component: Cart,
    props: true,
  },
  {
    path: "/favorites",
    name: "Favorites",
    component: Favorites,
    props: true,
  },
]
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'Error',
    component: Error
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from , next) => {
  console.log('Global beforeEach');
  console.log(to, from);
  if(to.fullPath === ' ') {
  next();
  }
  else {
    next({})
  }
  
});

router.afterEach((to, from) => {
  console.log('Global after each');
  console.log(to, from);
})

// router.beforeEach((to, from, next) => {
//   store.dispatch('fetchAccessToken');
//   if (to.fullPath === '/') {
//     console.log(store.state.token,"******")
//     if (!store.state.token) {
//       next('/error');
//     }
//   }
//   if (to.fullPath === '/login') {
//     if (store.state.token) {
//       next('/');
//     }
//   }
//   next();
// });



export default router
